package at.nmis;

public class HelloWorld {

	public static void main(String[] args) {
		System.out.println("Hello World");

		boolean res = isNumberEven(3);
		System.out.println(res);

		printCountDown(9);
	}

	public static boolean isNumberEven(int number) {
		if (number % 2 == 0) {
			return true;
		} else {
			return false;
		}
	}

	public static void printCountDown(int number) {
		while (number > 0) {
			System.out.println(--number);
		}
	}

}
