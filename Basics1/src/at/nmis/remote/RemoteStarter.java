package at.nmis.remote;

public class RemoteStarter {
	public static void main(String[] args) {
		
		Battery b1 = new Battery(66, "AA");
		Remote r1 = new Remote(400, 30, 30, 30, b1);
		Remote r2 = new Remote(300, 20, 20, 20, b1);
		
		
		r1.turnOn();
		r2.turnOff();

		r1.SayHello();
		r2.SayHello();
		
		System.out.println(b1.getBatteryStatus());
	}

}
