package at.nmis.remote;

public class Remote {
	private double weight;
	private double length;
	private double width;
	private double height;
	private boolean isOn = false;
	private Battery battery;
	

	public Remote(double w, double l, double wi, double he, Battery battery) {
		super();
		this.weight = w;
		this.length = l;
		this.width = wi;
		this.height = he;
		this.battery = battery;
	}

	public void turnOn() {
		System.out.println("I am turned on now");
		isOn = true;

	}

	public void turnOff() {
		System.out.println("I am turned off now");
		isOn = false;
	}
	
	public void SayHello() {
		System.out.println("HEIGHT " + this.height + " " + isOn);
	}
	
	

	public static void main(String[] args) {
//		Remote r1 = new Remote(400, 30, 30, 30);
//		Remote r2 = new Remote(300, 20, 20, 20);
//		r1.turnOn();
//		r2.turnOff();
//		
//		r1.SayHello();
//		r2.SayHello();
	}
}

