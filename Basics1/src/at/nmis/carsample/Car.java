package at.nmis.carsample;

public class Car {

	private String color;
	private int maxspeed;
	private int basicprice;
	private int basiccosumtion;
	private int power;
	private double price;
	private double consumtion;

	private Producer producer;
	private Engine engine;

	public Car(String color, int maxspeed, int basicprice, int basiccosumtion, Producer producer, Engine engine,
			int power) {
		super();
		this.color = color;
		this.maxspeed = maxspeed;
		
		double price = basicprice * (1 - producer.getDiscount());
		this.price = price;
		
		this.basicprice = basicprice;
		
		double consumtion = engine.getConsumtion();
		this.consumtion = consumtion;
		
		this.basiccosumtion = basiccosumtion;
		this.producer = producer;
		this.engine = engine;
		this.power = power;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getConsumtion() {
		return consumtion;
	}

	public void setConsumtion(double consumtion) {
		this.consumtion = consumtion;
	}
	
	public int getBasicprice() {
		return basicprice;
	}

	public int getPower() {
		return power;
	}

	public String getColor() {
		return color;
	}

	public int getMaxspeed() {
		return maxspeed;
	}

	public int getBasiccosumtion() {
		return basiccosumtion;
	}

}