package at.nmis.carsample;

public class CarStarter {

	public static void main(String[] args) {

		Producer p1 = new Producer(0.20);
		Engine e1 = new Engine(8, 40000);
		Car c1 = new Car("yellow", 200, 20000, 8, p1, e1, 300);
		Car c2 = new Car("green", 150, 30000, 3, p1, e1, 150);
		Person pe1 = new Person("Niklas", "Mischi", 16, "Austria");

		System.out.println("Farbe: " + c1.getColor());
		System.out.println("Maximalgeschwindigkeit: " + c1.getMaxspeed() + " km/h");
		System.out.println("Leistung: " + c1.getPower() + " PS");
		System.out.println("Endpreis: " + c1.getPrice() + " $");
		System.out.println("Verbrauch: " + c1.getConsumtion() + " Liter");

		System.out.println("Farbe: " + c2.getColor());
		System.out.println("Maximalgeschwindigkeit: " + c2.getMaxspeed() + " km/h");
		System.out.println("Leistung: " + c2.getPower() + " PS");
		System.out.println("Endpreis: " + c2.getPrice() + " $");
		System.out.println("Verbrauch: " + c2.getConsumtion() + " Liter");

		pe1.addCar(c1);
		pe1.addCar(c2);
		pe1.addCar(c1);

//		System.out.println(pe1.getCars());

		System.out.println("Anzahl der Autos in Besitz von pe1: " + pe1.getValueOfCars());
	}

}