package at.nmis.carsample;

public class Engine {

	private double consumtion;
	private double kilometers;

	public Engine(double consumtion, double kilometers) {
		super();

		if (kilometers > 50000) {
			consumtion = consumtion * 1.098;
		}

		this.consumtion = consumtion;
		this.kilometers = kilometers;
	}

	public double getKilometers() {
		return kilometers;
	}

	public void setKilometers(double kilometers) {
		this.kilometers = kilometers;
	}

	public double getConsumtion() {
		return consumtion;
	}

}