package at.nmis.carsample;

public class Producer {

	private double discount;

	public double getDiscount() {
		return discount;
	}

	public Producer(double discount) {
		super();
		this.discount = discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}
	
}