package at.nmis.carsample;

import java.util.ArrayList;

public class Person {

	private String firstname;
	private String lastname;
	private int age;
	private String country;

	private ArrayList<Car> cars;

	public Person(String firstname, String lastname, int age, String country) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.age = age;
		this.country = country;
		this.cars = new ArrayList<>();
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public ArrayList<Car> getCars() {
		return cars;
	}

	public void setCars(ArrayList<Car> cars) {
		this.cars = cars;
	}

	public void addCar(Car c) {
		this.cars.add(c);
	}

	int getValueOfCars() {
        int carSum = 0;
        for (Car car : cars) {
            carSum++;
        }
        return carSum;
    }

}
